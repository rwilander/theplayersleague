<?php

class User extends CActiveRecord
{
	public function tableName()
	{
		return 'users';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function attributeLabels()
	{
		return array(
			'email' => 'Email',
		);
	}

	public function relations()
	{
		return array(
			'summoner' => array(self::HAS_ONE, 'Summoner', 'userId'),
		);
	}

	public function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('email', 'unique'),
			array('email', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('id, email', 'safe', 'on'=>'search'),
		);
	}

	protected function beforeSave()
	{
		$preformValidate = parent::beforeSave();

		if (!empty($this->email)) {
			$this->email = strtolower($this->email);
		}

		return $preformValidate;
	}


	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	
}

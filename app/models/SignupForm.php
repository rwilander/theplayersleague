<?php

class SignupForm extends CFormModel
{
	public $name;
	public $email;
	public $captcha;

	private $user;
	private $summoner;

	function __construct($scenario = '')
	{
		$this->user = new User;
		$this->summoner = new Summoner;

		parent::__construct($scenario);
	}

	public function attributeLabels()
	{
		return array(
			'email' => 'Email',
			'name' => 'Summoner Name',
			'captcha' => 'Verification Code',
		);
	}

	public function rules()
	{
		return array(
			array('email', 'validateUserData'),
			array('name', 'validateSummonerData'),
			array('captcha', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
		);
	}

	public function validateSummonerData()
	{
		$this->summoner->name = $this->name;
		if (!$this->summoner->validate()) {
			$this->addErrors($this->summoner->errors);
		}
	}

	public function validateUserData()
	{
		$this->user->email = $this->email;
		if (!$this->user->validate()) {
			$this->addErrors($this->user->errors);
		}
	}

	public function signupUser()
	{
		if ($this->validate()) {
			$this->user->save();
			$this->summoner->userId = $this->user->id;
			$this->summoner->save();
		}

		return !$this->hasErrors();
	}
}
<?php

class Summoner extends CActiveRecord
{
	public function tableName()
	{
		return 'summoners';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Summoner Name',
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'summonerExists'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			// array('id, userId, name, profileIconId, profileIconSrc, revisionDate, summonerLevel', 'safe', 'on'=>'search'),
		);
	}

	public function summonerExists($attribute)
	{	
		if (!$this->getAndSetSummonerData($this->name)) {
			$this->addError($attribute, 'Summoner does not exist');
		}
	}

	protected function beforeSave()
	{
		$preformSave = parent::beforeSave();

		if (!empty($this->name)) {
			$this->name = strtolower($this->name);
		}

		if (empty($this->id) && $preformSave) {
			if (!$this->getAndSetSummonerData($this->name)) {
				$this->addError('name', 'Summoner does not exist');

				$preformSave = false;
			}
		}

		return $preformSave;
	}

	private function getAndSetSummonerData()
	{
		$leagueApi = new LeagueApi;
		$reponse = $leagueApi->getSummonersByName($this->name);

		$summonerData = null;
		if ($reponse->code === LeagueApi::HTTP_OK) {
			$summonerData = $reponse->body->{strtolower($this->name)};

			$this->setAttributes((array) $summonerData, false);
		}

		return $summonerData;
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('profileIconId',$this->profileIconId);
		$criteria->compare('profileIconSrc',$this->profileIconSrc,true);
		$criteria->compare('revisionDate',$this->revisionDate);
		$criteria->compare('summonerLevel',$this->summonerLevel);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

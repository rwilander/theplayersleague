<?php

class SiteController extends Controller
{
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionSignup()
	{
		$signupForm = new SignupForm;

		if (isset($_POST['SignupForm'])) {
			$signupForm->attributes = $_POST['SignupForm'];

			if ($signupForm->validate() && $signupForm->signupUser()) {
				Yii::app()->user->setFlash('success', 'Thanks for signing up!<br/><br/>We have sent a configmation email to :<br/><strong>' . 'randy.wilander@gmail.com' . '</strong>');
				$this->refresh();	
			}			
		}				

		$this->render('signup', array('model' => $signupForm));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}
<?php

class m140905_192541_initialSchema extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', array(
			'id' => 'pk',			
			'email' => 'string NOT NULL',
		));

		$this->createTable('summoners', array(
			'id' => 'integer NOT NULL PRIMARY KEY',
			'userId' => 'integer NOT NULL',
			'name' => 'string NOT NULL',
			'profileIconId' => 'integer NOT NULL',
			'profileIconSrc' => 'string NOT NULL',
			'revisionDate' => 'integer NOT NULL',
			'summonerLevel' => 'integer NOT NULL',
		));
		
		$this->addForeignKey(
			'fk_summoners_userId',
			'summoners',
			'userId',
			'users',
			'id'
		);

		$this->createIndex('users_index_id', 'users', 'id');
		$this->createIndex('users_index_email', 'users', 'email');
		$this->createIndex('summoners_index_id', 'summoners', 'id');
		$this->createIndex('summoners_index_name', 'summoners', 'name');
	}

	public function down()
	{
		$this->dropTable('summoners');
		$this->dropTable('users');
	}
}

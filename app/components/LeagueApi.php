<?php
/**
 * TODO: 
 *	1) Add a function to test if the service is working
 *	2) Add fallbacks when requests fail
 */

class LeagueApi extends CComponent
{
	/**
	 * League API Variables
	 */ 
	private $apiKey 	 = '';
	private $requestData = array();

	/**
	 * Server/Endpoint Variables
	 */
	private $serverUrl = 'https://{region}.api.pvp.net';
	private $endpoints = array(
		'summoner' => array(
			'baseUrl'	=> 'api/lol/{region}/v{version}/summoner',
			'byName' 	=> 'by-name/{summonerNames}',
			'byId' 		=> '{summonerIds}',
			'masteries' => '{summonerIds}/masteries',
			'name' 		=> '{summonerIds}/name',
			'runes' 	=> '{summonerIds}/runes',
		),
		'league' => array(
			'baseUrl' => '/api/lol/{region}/v{version}/league',
			'entry-by-summoner' => 'by-summoner/{summonerIds}/entry',
		),
		'static-data' => array(
			'baseUrl' 	=> 'api/lol/static-data/{region}/v{version}',
			'versions' 	=> 'versions',
		),
		'cdn' => array (
			'baseUrl' => 'http://ddragon.leagueoflegends.com/cdn/{version}',
			'profileicon' => 'img/profileicon/{iconId}.png',
		),
	);

	/**
	 * Request/Response Variables
	 */
	private $request  = '';
	private $response = null;

	/**
	 * Http responses
	 */
	const HTTP_OK 					= 200;
	const HTTP_BAD_REQUEST 			= 400;
	const HTTP_UNAUTHORIZED 		= 401;
	const HTTP_NOT_FOUND 			= 404;
	const HTTP_RATE_LIMIT_EXCEDED 	= 429;
	const HTTP_SERVER_ERROR 		= 500;
	const HTTP_SERVICE_UNAVALABLE 	= 503;

	function __construct()
	{
        $region = 'na';
        $arguments = func_get_args();
        
        if (isset($arguments['region'])) {
        	$region = $arguments['region'];
        }

		$this->setRequestData('region', $region);
		$this->apiKey = Yii::app()->params['leagueApiKey'];
	}

	/**
	 * League of Ledgends interface functions
	 */
	public function getSummonersByName($summonerNames)
	{
		if (is_array($summonerNames)) {
			$summonerNames = implode(',', $summonerNames);
		}

		$this->setRequestDataArray(array(
			'version' => '1.4',
			'summonerNames' => $summonerNames
		));

		$response = $this->sendRequest('summoner', 'byName');

		return $this->processSummoners($response);
	}

	public function getSummonersById($summonerIds)
	{
		if (is_array($summonerIds)) {
			$summonerIds = implode(',', $summonerIds);
		}

		$this->setRequestDataArray(array(
			'version' => '1.4',
			'summonerIds' => $summonerIds
		));
		
		$response = $this->sendRequest('summoner', 'byId');

		return $this->processSummoners($response);
	}

	public function getSummonerLeagueEntriesById($summonerIds, $allowedQueues = array()) {
		if (is_array($summonerIds)) {
			$summonerIds = implode(',', $summonerIds);
		}

		$this->setRequestDataArray(array(
			'version' => '2.5',
			'summonerIds' => $summonerIds
		));
		
		$response = $this->sendRequest('league', 'entry-by-summoner');

		return $this->processSummonerLeagueEntries($response, $allowedQueues);
	}

	public function getGameVersions()
	{
		$this->setRequestData('version', '1.2');
		
		return $this->sendRequest('static-data', 'versions');
	}

	public function getProfileIconSrcById($iconId)
	{
		$this->setRequestDataArray(array(
			'version' => $this->getGameVersions()->body[0],
			'iconId' => $iconId
		));
		
		$this->buildRequest('cdn', 'profileicon', false);

		return $this->request;
	}

	/**
	 * Endpoint helpers
	 */
	private function processSummoners($response)
	{
		if ($response->code !== self::HTTP_OK) {
			return $response;
		}

		foreach ($response->body as $summoner => &$data) {
			$data->revisionDate /= 1000; 
			$data->profileIconSrc = $this->getProfileIconSrcById($data->profileIconId);
		}

		return $response;
	}

	private function processSummonerLeagueEntries($response, $allowedQueues)
	{
		if ($response->code !== self::HTTP_OK) {
			return $response;
		}

		$processedLeagueEntries = (object) null;
		
		foreach ($response->body as $summonerId => $leagues) {
			$processedLeagueEntries->{$summonerId} = array();

			foreach ($leagues as $league) {
				$processedLeague = $league;

				if (count($allowedQueues) && !in_array($league->queue, $allowedQueues)) {
					continue;
				}

				foreach ($league->entries[0] as $key => $value) {
					$processedLeague->{$key} = $value;
				}
				
				unset($processedLeague->entries);

				$processedLeagueEntries->{$summonerId}[] = $processedLeague;
			}
		}

		$response->body = $processedLeagueEntries;

		return $response;
	}

	/**
	 * Request/Response helper functions
	 */
	private function setRequestData($needle, $replacement)
	{
		$needle = '{' . $needle . '}';
		$this->requestData[$needle] = $replacement;
	}

	private function setRequestDataArray($requestData)
	{
		foreach ($requestData as $needle => $replacement) {
			$this->setRequestData($needle, $replacement);
		}
	}

	private function sendRequest($endpoint, $action)
	{
		$this->buildRequest($endpoint, $action);

		$options = array(
			'http' => array(
		        'method'  => 'GET',
		        'header'  => 
		            'Accept-Language: en-US' . "/r/n" .
					'Accept-Charset: ISO-8859-1,utf-8' . "/r/n",
				'ignore_errors' => true,
	    	)
		);
		$context = stream_context_create($options);

		return $this->parseResponse(
			file_get_contents($this->request, false, $context),
			$http_response_header
		);
	}

	private function buildRequest($endpoint, $action, $includeApiKey = true)
	{
		$requestParts = array(
			$this->serverUrl,
			$this->endpoints[$endpoint]['baseUrl'],
			$this->endpoints[$endpoint][$action]
		);

		if ($endpoint === 'cdn') {
			unset($requestParts[0]);
		}

		$request = implode('/', $requestParts);
		if ($includeApiKey) {
			$request .= '?' . http_build_query(array('api_key' => $this->apiKey));
		}

		$needles = array_keys($this->requestData);
		$replacements = array_values($this->requestData);
		
		$this->request = str_replace($needles, $replacements, $request);
	}

	protected function parseResponse($responseBody, $headers)
	{
		$status = explode(' ', $headers[0]);

		if ($status[1] == self::HTTP_OK) {
			$responseBody = json_decode($responseBody);
		}

		$response = (object) null;
		$response->body = $responseBody;
		$response->code = (int) $status[1];
		$response->headers = $headers;
		$response->message = implode(' ', array_slice($status, 2));

		return $response;
	}
}
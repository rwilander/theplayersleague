<?php 
/**
 * TODO: Remove temparary inline CSS below
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<title><?php echo Yii::app()->name; ?></title>
</head>
<style>
	#ui-header {
		height: 70px;
		position: fixed;
		padding: 0 20px;
		top: 0;
		left: 0;
		right: 0;
		background: white;
		z-index: 2;
		-webkit-box-shadow: 0px 1px 5px rgba(50,50,50,0.6);
		-moz-box-shadow: 0px 1px 5px rgba(50,50,50,0.6);
		box-shadow: 0px 1px 5px rgba(50,50,50,0.6);
	}
	#ui-header h1 {
		float: left;
		font-size: 40px;
		margin-top: 15px;
	}
	#site-nav {
		height: 100%;
		float: right;
	}
	#site-nav li {
		height: 100%;
		width: 150px;
		margin: 0;
	}
	#site-nav li a {
		border-radius: 0;
		height: 100%;
		display: block;
		padding-top: 33px;
		font-size: 20px;
		text-align: center;
	}
	#ui-main {
		margin-top: 70px;
	}
	#ui-main .container-fluid {
		padding: 0;
	}
	#ui-footer {	
		color: white;
		padding: 15px 20px;
		background-color: #2e2e2e;
	}
	#ui-footer ul {	
		padding: 0;
		float: left;
		width: 162px;
		list-style: none;
		overflow: hidden;
	}
	#ui-footer ul li {	
		float: left;
		margin-right: 20px;
	}
	#ui-footer p {
		float: right;
	}
	.container {
		max-width: 700px;
	}
	/* sticky Footer */
	html,
	body {
		margin:0;
		padding:0;
		height:100%;
	}
	#ui-wrapper {
		min-height:100%;
		position:relative;
	}
	#ui-main {
		padding-bottom: 60px;
	}
	#ui-footer {
		width: 100%;
		height: 60px;
		position: absolute;
		bottom: 0;
		left: 0;
	}
</style>
<body>
<div id="ui-wrapper">
	<header id="ui-header">
		<h1><?php echo Yii::app()->name; ?></h1>
		<?php 
			$this->widget('zii.widgets.CMenu', array(
			    'id' => 'site-nav',
			    'items'=>array(
			        array('label'=>'Home', 'url'=>array('site/index')),
			        array('label'=>'Signup', 'url'=>array('site/signup')),
			    ),
			    'htmlOptions' => array(
			    	'class' => 'nav nav-pills',
			    )
			));
		?>
		<div class="clearfix"></div>
	</header>
	<div id="ui-main">
		<div class="container-fluid">
			<?php echo $content; ?>
		</div>
	</div>
	<footer id="ui-footer">
		<?php 
			$this->widget('zii.widgets.CMenu', array(
			    'id' => 'footer-nav',
			    'items'=>array(
			        array('label'=>'Home', 'url'=>array('site/index')),
			        array('label'=>'Signup', 'url'=>array('site/signup')),
			    )
			));
		?>
		<p>Designed and developed by <a href="http://rwwebdesign.ca">rwwebdesign.ca</a></p>
		<div class="clearfix"></div>
	</footer>
</div>
</body>
</html>

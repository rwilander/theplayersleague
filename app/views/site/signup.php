<?php
/**
 * TODO: Remove temparary inline CSS below
 */
?>

<style>
	.alert {
		height: 115px;
		max-width: 400px;
		margin: 0 auto;
		margin-top: 15px;
	}
	h2 {
		margin-top: 35px;
		text-align: center;
	}
	.signup-form {
		margin: 0 auto;
		float: inherit;
		max-width: 400px;
		margin-bottom: 50px;
	}
	.submit {
		margin: 0 auto;
		float: inherit;
		padding: 0;
	}
	.submit input {
		width: 100%;
	}
	.captcha .form-control-feedback {
		top: 75px;
	}
</style>
<?php if(Yii::app()->user->hasFlash('success')): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
<?php endif; ?>
<h2>Beta Signup</h2>
<div class="signup-form">
	<p>Ut feugiat nulla in fermentum elementum. Sed sodales quam vitae enim pharetra porttitor. Aenean et fringilla dui. Interdum et malesuada fames ac.</p>
	<?php $form=$this->beginWidget('CActiveForm'); ?>
		<?php foreach ($model->safeAttributeNames as $input): ?>
			<?php 
				$hasErrors = $model->hasErrors($input); 
				$hasValidInput = !$hasErrors && $model->{$input} != '';
				$feebackClass = '';
				if ($hasValidInput) {
					$feebackClass = 'has-success';
				} else if ($hasErrors){
					$feebackClass = 'has-error';
				}
			?>
			<div class="form-group has-feedback <?php echo $feebackClass . ' ' . $input; ?>">
				<?php echo $form->labelEx($model, $input, array('class' => 'control-label')); ?>
				<?php if(CCaptcha::checkRequirements() && $input === 'captcha'): ?>
					<div>
						<?php $this->widget('CCaptcha'); ?>
					</div>
				<?php endif; ?>
				<?php echo $form->textField($model, $input, array('class' => 'form-control')); ?>
				<?php if ($hasErrors || $hasValidInput): ?>
					<span class="glyphicon <?php echo $hasErrors ? 'glyphicon-remove' : 'glyphicon-ok'; ?> form-control-feedback"></span>
				<?php endif; ?>
				<?php echo $form->error($model, $input); ?>	
				<?php if ($input === 'captcha'): ?>
					<div class="help-block">Please enter the letters as they are shown in the image above. <br/>Letters are not case-sensitive.</div>
				<?php endif;?>
			</div>
		<?php endforeach; ?>
		<div class="submit">
			<?php echo CHtml::submitButton('Signup', array('class' => 'btn btn-primary')); ?>
		</div>
	<?php $this->endWidget(); ?>
</div>
<div class="clearfix"></div>

<?php
/**
 * TODO: Remove temparary inline CSS below
 */
?>

<style>
	#cta {
		height: 500px;
		background: url(http://fc00.deviantart.net/fs70/f/2014/007/2/7/league_of_legends__clashing_winds_by_ae_rie-d70x719.png) no-repeat center center fixed; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		border-bottom: 3px solid white;
		-webkit-box-shadow: 0px 1px 5px rgba(50,50,50,0.6);
		-moz-box-shadow: 0px 1px 5px rgba(50,50,50,0.6);
		box-shadow: 0px 1px 5px rgba(50,50,50,0.6);	
	}
	#cta .signup {
		float: right;
		padding: 20px;
		width: 320px;
		margin-top: 283px;
		margin-right: 10px;
		background: rgba(255,255,255,.5);
	}
	#cta .signup h2 {
		margin-top: 0;
	}
	#about {
		margin: 30px 0;
	}
	#contact {
		color: white;
		padding: 30px 0;
		background-color: #535353;
	}
</style>
<section id="cta">
	<div class="signup">
		<h2>Beta Signup</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet massa sem, nec porta diam pulvinar vel. In euismod viverra placerat.</p>
		<?php 
			echo CHtml::link(
				'Signup', 
				$this->createAbsoluteUrl('site/signup'),
				array('type' => "button",'class' => "btn btn-primary")
			); 
		?>
	</div>
</section>
<section id="about">
	<div class="container">
		<div>
			<h3>What is The Player League?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id mattis dui. In eget tellus nibh. Curabitur dolor lacus, maximus commodo diam sed, congue luctus odio. Praesent suscipit magna nec rutrum porttitor. Duis placerat augue eu suscipit vulputate. Aliquam vulputate nibh augue, ac ornare velit viverra sit amet. Nullam orci dui, lobortis non tortor non, interdum cursus ligula. Etiam viverra eget ante vitae suscipit. Maecenas sit amet turpis at dui scelerisque interdum aliquam iaculis quam.</p>
			<p>Nullam commodo, orci eu tincidunt eleifend, lectus mi ornare orci, non sodales eros dolor id velit. Cras tincidunt libero scelerisque urna blandit mollis. Ut tristique id nulla at iaculis. Fusce ut sapien eget neque cursus aliquam non sit amet metus. Sed pulvinar porttitor faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur efficitur augue quis mi suscipit, in pretium velit volutpat. Suspendisse potenti. Duis pulvinar, neque mattis hendrerit iaculis, quam ipsum luctus mauris, vel ornare tellus ante eu risus. Nullam quis bibendum sapien, a varius felis. Nam aliquam augue justo, eget porttitor metus malesuada varius. Duis consectetur ultrices odio, a euismod sem tincidunt eu. Suspendisse eleifend ultrices malesuada. Donec pulvinar maximus diam rhoncus suscipit. Aenean sodales quam sit amet lorem condimentum finibus.</p>
		</div>
		<div>
			<h3>Are there cash prizes?</h3>
			<p>Ut feugiat nulla in fermentum elementum. Sed sodales quam vitae enim pharetra porttitor. Aenean et fringilla dui. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed fermentum elit ac leo consectetur feugiat. Nullam ut leo nunc. Praesent sit amet nisi feugiat, lacinia urna at, auctor orci.</p>
		</div>
		<div>
			<h3>Is there Beer?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id mattis dui. In eget tellus nibh. Curabitur dolor lacus, maximus commodo diam sed, congue luctus odio. Praesent suscipit magna nec rutrum porttitor.</p>			
		</div>
		<div>
			<h3>Sounds good! When does it start?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur efficitur augue quis mi suscipit, in pretium velit volutpat. Suspendisse potenti. Duis pulvinar, neque mattis hendrerit iaculis, quam ipsum luctus mauris, vel ornare tellus ante eu risus. Nullam quis bibendum sapien, a varius felis. Nam aliquam augue justo, eget porttitor metus malesuada varius</p>			
		</div>
		<div>
			<h3>How do I sign up?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur efficitur augue quis mi suscipit, in pretium velit volutpat. Suspendisse potenti. Duis pulvinar, neque mattis hendrerit iaculis, quam ipsum luctus mauris, vel ornare tellus ante eu risus. Nullam quis bibendum sapien, a varius felis. Nam aliquam augue justo, eget porttitor metus malesuada varius</p>			
			<?php 
				echo CHtml::link(
					'Click here to signup!', 
					$this->createAbsoluteUrl('site/signup'),
					array('type' => "button",'class' => "btn btn-primary")
				); 
			?>
		</div>	
	</div>
</section>
<section id="contact">
	<div class="container">
		<h3>Questions or Comments?</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus mi ornare orc? Augue quis mi et fringilla dui. Praesent id mattis dui. In eget tellus nibh. Curabitur dolor lacus.</p>
		<p><a href="mailto:info@tpl.gg">Email Us</a> ed sodales quam vitae enim pharetra.</p>		
	</div>
</section>
